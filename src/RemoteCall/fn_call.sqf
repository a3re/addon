params ["_func", "_args"];

private _connected = call A3RE_fnc_isConnected;
if (!_connected) then {
	diag_log "Detected disconnection during call";

	call A3RE_fnc_disconnect;
	private _status = call A3RE_fnc_connect;
	if (!_status) exitWith {
		diag_log "---------------------------------------------------------------------";
		diag_log "FATAL ERROR! Can't reconnect to A3RE Query server!";
		diag_log format ["Failed to call %1! No connection with A3RE Query Server.", _func];
		diag_log "---------------------------------------------------------------------";

		["", -1, 0]
	};
};

private _remoteFunc = format ["remote(%1)", _func];
"A3RE" callExtension [_remoteFunc, _args];