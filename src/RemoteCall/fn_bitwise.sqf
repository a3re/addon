params ["_a", "_b"];

private _ret = "A3RE" callExtension ["bitwise", [_a, _b]];
_status = _ret select 1;

if (_status == 0) then {
	parseNumber (_ret select 0);
} else {
	0;
};