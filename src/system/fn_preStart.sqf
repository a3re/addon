diag_log "---------------------------------------------------------------------";
private _result = "A3RE" callExtension "VERSION";
if (_result == "") then {
    diag_log "failed to load Arma 3 Remote Executor";
    A3RE_var_loaded = false;
} else {
    diag_log "Arma 3 Remote Executor loaded";
    A3RE_var_loaded = true;

    private _ret = "A3RE" callExtension ["connect", []];
    private _status = _ret select 1;

    if (_status == 0) then {
        diag_log "Successfully connected to A3RE Query server";
    } else {
        diag_log "Can't connect to A3RE Query server";
    };
};
diag_log "---------------------------------------------------------------------";