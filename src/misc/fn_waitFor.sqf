params ["_statementCode", "_args", "_maxAttempts"];

private _desc = _this spawn {
	params ["_statementCode", "_args", "_maxAttempts"];

	_attempts = 0;
	_maxAttempts = 5;
	while {_attempts < _maxAttempts} do {
		_attempts = _attempts + 1;

		if (_args call _statementCode) then { break; };
		sleep 0.1;
	};
};

while {!(scriptDone _desc)} do { };

_args call _statementCode;
