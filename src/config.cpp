class CfgPatches {
	class A3RE {
		projectName = "Arma 3 Remote Executor";
		author = "Bit";
		version = "v0.1";
		requiredAddons[] = {};
		units[] = {};
	};
};

class CfgFunctions {
	class A3RE {
		class misc {
			file = "A3RE\misc";
			class waitFor {};
		};

		class system {
			file = "A3RE\system";
			class isLoaded {};
			class preStart {
				preStart = 1;
			};
		};

		class RemoteCall {
			file = "A3RE\RemoteCall";
			class call {};
			class isConnected {};

			class connect {};
			class disconnect {};

			class version {};
		};
	};
};
