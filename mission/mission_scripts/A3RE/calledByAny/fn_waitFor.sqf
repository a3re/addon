params ["_statementCode", "_args", "_maxAttempts", ["_callback", {}], ["_async", false]];

private _desc = [_statementCode, _args, _maxAttempts, _callback, _async] spawn {
	params ["_statementCode", "_args", "_maxAttempts", "_callback", "_async"];

	private _res = false;
	private _attempts = 0;
	while {_attempts < _maxAttempts} do {
		_attempts = _attempts + 1;

		_res = _args call _statementCode;
		if (_res) then { break; };
		sleep 0.1;
	};

	[_res, _async, _args] call _callback;
};

if (!_async) then {
	while {!(scriptDone _desc)} do { };

	_args call _statementCode;
} else { _desc };
