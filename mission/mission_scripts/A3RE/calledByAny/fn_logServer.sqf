private _netID = clientOwner;

if (_netID == 2) then {
	(format ["[SERVER] %1", _this]) remoteExec ["diag_log", 2];
} else {
	(format ["[CLIENT<%1>] %2", _netID, _this]) remoteExec ["diag_log", 2];
};