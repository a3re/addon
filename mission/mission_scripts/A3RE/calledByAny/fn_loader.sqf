params ["_type", "_data", "_args", "_callback"];

[[_type, _data, _args, _callback, clientOwner], {
	params ["_type", "_data", "_args", "_callback", "_client"];

	private _res = ["loader", [_type, _client, _data]] call A3RE_fnc_call;

	private _res_data = (_res select 0);
	private _status = (_res select 1);

	if (_status == 0) then {
		[[_res_data, _args], _callback, _client] call A3RE_M_fnc_remoteCall;
	} else {
		diag_log format ["(%2, %3, %4) An error occurred in the loader: %1", _res_data, _type, _data, _client];
	};
}, 2] call A3RE_M_fnc_remoteCall;