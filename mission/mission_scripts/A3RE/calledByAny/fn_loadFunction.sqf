params ["_function", ["_async", false]];

[{
	params ["_function", "_args"];

	[1, _function, _function, A3RE_M_fnc_compile] call A3RE_M_fnc_loader;
}, {
	params ["_res", "_async", "_args"];
	_args params ["_function", "_args2"];

	private _typeStr = if (_async) then { "async" } else { "sync" };

	if (_res) then {
		(format ["[%1] Loaded function %2", _typeStr, _function]) call A3RE_M_fnc_logServer;
	} else {
		(format ["[%1] Failed to load function %2", _typeStr, _function]) call A3RE_M_fnc_logServer;
	};
}, _function, [], 10, _async] call A3RE_M_fnc_waitForLoading;