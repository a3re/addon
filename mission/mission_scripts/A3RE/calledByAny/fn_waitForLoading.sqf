params ["_loading", "_callback", "_name", "_args", ["_attempts", 10], ["_async", false]];

private _isLoaded = _name call A3RE_M_fnc_isLoaded;

if (!_isLoaded) then {
	[_name, _args] call _loading;
	_isLoaded = [A3RE_M_fnc_isLoaded, [_name, _args], _attempts, _callback, _async] call A3RE_M_fnc_waitFor;
};

_isLoaded