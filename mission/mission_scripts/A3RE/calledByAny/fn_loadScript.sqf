params ["_script", ["_async", false]];
private _name = format ["script_%1", _script];

[{
	params ["_name", "_args"];
	(_args select 1) params ["_script"];

	[2, _script, _name, A3RE_M_fnc_compile] call A3RE_M_fnc_loader;
}, {
	params ["_res", "_async", "_args"];
	(_args select 1) params ["_script"];

	private _typeStr = if (_async) then { "async" } else { "sync" };

	if (_res) then {
		(format ["[%1] Loaded script %2", _typeStr, _script]) call A3RE_M_fnc_logServer;
	} else {
		(format ["[%1] Failed to load script %2", _typeStr, _script]) call A3RE_M_fnc_logServer;
	};
}, _name, [_script], 10, _async] call A3RE_M_fnc_waitForLoading;