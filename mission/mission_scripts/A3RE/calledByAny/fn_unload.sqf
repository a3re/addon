params ["_name"];

private _isLoaded = _name call A3RE_M_fnc_isLoaded;

if (_isLoaded) then {
	missionNamespace setVariable [_name, nil];

	true
} else { 
	false 
};