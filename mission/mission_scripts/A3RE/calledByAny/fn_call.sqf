params ["_name", ["_args", []], ["_async", false]];

private _isLoaded = _name call A3RE_M_fnc_isLoaded;

if (!_isLoaded) then {
	_isLoaded = [A3RE_M_fnc_isLoaded, _name, 10] call A3RE_M_fnc_waitFor;
};

if (!_isLoaded) then {
	(format ["Failed to call %1", _name]) call A3RE_M_fnc_logServer;
} else {
	private _code = missionNamespace getVariable _name;

	if (_async) then {
		[_args, _code] spawn {
			params ["_args", "_code"];

			_args call _code;
		};
	} else {
		_args call _code;
	};
};