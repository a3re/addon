private _loadList = missionNamespace getVariable ["loadList", nil];

{
	_x params ["_name", "_isShared", "_isScript"];

	if (_isScript) then {
		[_name, true] call A3RE_M_fnc_loadScript;
	} else {
		[_name, true] call A3RE_M_fnc_loadFunction;
	};
} forEach _loadList;

{
	_x params ["_name", "_isShared", "_isScript"];

	if (!_isScript) then {
		[A3RE_M_fnc_isLoaded, _name, 10] call A3RE_M_fnc_waitFor;
	};
} forEach _loadList;