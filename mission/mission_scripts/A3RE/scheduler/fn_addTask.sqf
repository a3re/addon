params ["_interval", "_args", "_code", ["_oneShot", true]];

private _schedulerTasks = missionNamespace getVariable ["A3RE_schedulerTasks", []];
private _taskName = format ["task_%1", str round (random [0, 50000, 100000])];
diag_log format ["Adding task %1 (interval %2, oneShot %3)", _taskName, _interval, _oneShot];
_schedulerTasks pushBack [_taskName, time, _interval, _args, _code, _oneShot];
missionNamespace setVariable ["A3RE_schedulerTasks", _schedulerTasks];

_taskName