
private _schedulerTasks = missionNamespace getVariable ["A3RE_schedulerTasks", []];

private _taskPos = _schedulerTasks findIf { (_x select 0) == _this };
if (taskPos != -1) then {
	private _task = _schedulerTasks select _taskPos;
	_task params ["_taskName", "_interval", "_args", "_code", "_oneShot"];
	diag_log format ["Deleting task %1 (interval %2, oneShot %3)", _taskName, _interval, _oneShot];
	_schedulerTasks deleteAt _taskPos;
	missionNamespace setVariable ["A3RE_schedulerTasks", _schedulerTasks];
}