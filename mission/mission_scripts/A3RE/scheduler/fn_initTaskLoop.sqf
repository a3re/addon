onEachFrame {
	private _curTime = time;
	private _schedulerTasks = missionNamespace getVariable ["A3RE_schedulerTasks", []];

	private _execQueue = _schedulerTasks select {
		_x params ["_taskName", "_lastTime", "_interval", "_args", "_code", "_oneShot"];
		(_curTime - _lastTime) > _interval
	};

	{
		_x params ["_taskName", "_lastTime", "_interval", "_args", "_code", "_oneShot"];
		
		[_taskName, _args] spawn _code;

		if (_oneShot) then {
			_taskName call A3RE_M_fnc_delTask;
		} else {
			_x set [1, _curTime];
			_schedulerTasks set [_forEachIndex, _x];
		};
	} forEach _execQueue;

	if (count _execQueue > 0) then {
		missionNamespace setVariable ["A3RE_schedulerTasks", _schedulerTasks];
	};
};