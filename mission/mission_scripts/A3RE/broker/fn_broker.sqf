private _args = _this + [player, clientOwner];

[_args, {
	params ["_func", "_data", "_args", "_callback", "_player", "_client"];

	private _name = if (isNull _player) then { "!!SERVER!!" } else { name _player };
	private _uid = if (isNull _player) then { "!!SERVER!!" } else { getPlayerUID _player };

	private _res = ["broker", [_func, _client, _name, _uid] + _data] call A3RE_fnc_call;

	private _res_data = (_res select 0);
	private _status = (_res select 1);

	if (_status == 0) then {
		[[_res_data, _args], _callback, _client] call A3RE_M_fnc_remoteCall;
	} else {
		diag_log format ["(%2, %3, %4) An error occurred in the broker: %1", _res_data, _type, _data, _client];
	};
}, 2] call A3RE_M_fnc_remoteCall;