class A3RE_M {
	class Any { 
		file = "A3RE\calledByAny";
		class waitFor {};
		class waitForLoading {};

		class remoteCall {};

		class loader {};
		class handle {};

		class callFunction {};
		class loadFunction {};

		class callScript {};
		class loadScript {};

		class unload {};
		class isLoaded {};

		class call {};
		class callWithoutArgs {};
		class makeShared {};

		class logServer {};
	};

	class Server { 
		file = "A3RE\calledByServer";
		class execute {};
		class compile {};
	};

	class Client { 
		file = "A3RE\calledByClient";
	};

	class Handlers {
		file = "A3RE\handlers";
		class handle_init {};
		class handle_initIntro {};
		class handle_initServer {};
		class handle_initPlayerLocal {};
		class handle_initPlayerServer {};

		class handle_onPlayerKilled {};
		class handle_onPlayerRespawn {};
	};

	class Scheduler {
		file = "A3RE\scheduler";
		class initTaskLoop {};
		class addTask {};
		class delTask {};
	};

	class Loader {
		file = "A3RE\loader";
		class generateLoadList {};
		class load {};
		class share {};
	};

	class Broker {
		file = "A3RE\broker";
		class broker {};
		class getPlayer {};
	};
};